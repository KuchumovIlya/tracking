__author__ = 'ilyakuchumov'

from point import Point2D
from copy import deepcopy


def get_matrix_wh(matrix):
    return len(matrix[0]), len(matrix)


def create_array(length, init_value=0):
    return [init_value for x in range(length)]


def create_matrix(width, height, init_value=0):
    return [[init_value for j in range(width)] for i in range(height)]


def is_in_matrix(point, matrix):
    width, height = get_matrix_wh(matrix)
    return 0 <= point.x < height and 0 <= point.y < width


def _dfs(v, value, matrix, used, cur_component):
    used[v.x][v.y] = True
    q = [v]

    while len(q) != 0:
        v = q[-1]
        q.pop()
        cur_component.append(v)

        for dx in [-1, 0, 1]:
            for dy in [-1, 0, 1]:
                new_v = v + Point2D(dx, dy)
                if is_in_matrix(new_v, matrix) and matrix[new_v.x][new_v.y] == value and not used[new_v.x][new_v.y]:
                    used[new_v.x][new_v.y] = True
                    q.append(new_v)


def find_connected_components(matrix, value):
    width, height = get_matrix_wh(matrix)
    used = create_matrix(width, height, False)
    components = []
    for i in range(height):
        for j in range(width):
            if used[i][j] or matrix[i][j] != value:
                continue
            cur_component = []
            _dfs(v=Point2D(i, j), value=value, matrix=matrix, used=used, cur_component=cur_component)
            components.append(cur_component)
    return components


def do_majority_fix_on_01_matrix(matrix):
    matrix = deepcopy(matrix)
    height = len(matrix)
    width = len(matrix[0])
    for it in range(5):
        for i in range(height):
            for j in range(width):
                ones_cnt = 0
                for dx in [-1, 0, 1]:
                    for dy in [-1, 0, 1]:
                        new_i = i + dx
                        new_j = j + dy
                        if 0 <= new_i < height and 0 <= new_j < width:
                            ones_cnt += matrix[new_i][new_j]
                matrix[i][j] = 1 if ones_cnt >= 5 else 0
    return matrix