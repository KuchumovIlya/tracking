__author__ = 'ilyakuchumov'

from point import Point2D, Point3D
from copy import deepcopy
from PIL import ImageFont
from PIL import ImageDraw


nice_colors = [Point3D(0, 0, 255), Point3D(204, 51, 255), Point3D(0, 255, 153), Point3D(255, 102, 0),
               Point3D(255, 255, 0), Point3D(255, 0, 0), Point3D(204, 153, 0), Point3D(0, 255, 255),
               Point3D(51, 102, 255), Point3D(255, 0, 102)]


class Blob:
    def __init__(self, track_id, mean_color, center, points):
        self.track_id = track_id
        self.mean_color = mean_color
        self.center = center
        self.points = points

    def can_merge_with(self, other):
        diff_color = other.mean_color - self.mean_color
        diff_dist = other.center - self.center
        possible_dist = max(len(self.points), len(other.points)) // 5
        return (abs(diff_color.x) <= 20 and abs(diff_color.y) <= 20 and abs(diff_color.z) <= 20 and
                abs(diff_dist.x) <= possible_dist and abs(diff_dist.y) <= possible_dist
        )

    def merge_with(self, other):
        size1 = len(self.points)
        size2 = len(other.points)
        self.mean_color = (self.mean_color * size1 + other.mean_color * size2) / (size1 + size2)
        self.center = (self.center * size1 + other.center * size2) / (size1 + size2)
        self.points += other.points

    def put_on_image(self, image):
        image = deepcopy(image)

        nice_color = nice_colors[self.track_id % len(nice_colors)]
        # x, y or y, x?
        for point in self.points:
            image.putpixel((point.y, point.x), (nice_color.x, nice_color.y, nice_color.z))

        font = ImageFont.truetype("./font.ttf", 30)
        draw = ImageDraw.Draw(image)
        draw.text((self.center.y, self.center.x), str(self.track_id), (255, 255, 255), font=font)
        return image