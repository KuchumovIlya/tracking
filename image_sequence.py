__author__ = 'ilyakuchumov'

from image_utils import open_image, get_pixels_intensity
from matrix_utils import find_connected_components
from point import Point2D, Point3D
from blob import Blob
import matrix_utils


def _get_all_files_in_folder(folder_path, files_limit):
    from os import listdir
    from os.path import isfile, join
    only_files = [f for f in listdir(folder_path) if isfile(join(folder_path, f))]
    only_files = [f for f in only_files if f[0] != '.']
    only_files = [join(folder_path, f) for f in only_files]
    only_files = only_files[:min(files_limit, len(only_files))]
    return only_files


class ImageSequence:
    def __init__(self, folder_path, files_limit=30, n=30, t1=20, t2=30, min_blob_size=50):
        self.n = n
        self.t1 = t1
        self.t2 = t2
        self.min_blob_size = min_blob_size

        self.files_names = _get_all_files_in_folder(folder_path, files_limit)
        print(self.files_names)

        self.size = len(self.files_names)
        self.images = []
        self.images_intensities = []
        self.images_rgb_matrices = []
        for image_path in self.files_names:
            image, rgb_matrix = open_image(image_path)
            self.images.append(image)
            self.images_rgb_matrices.append(rgb_matrix)
            intensity_matrix = get_pixels_intensity(rgb_matrix)
            self.images_intensities.append(intensity_matrix)

    def get_bg(self):
        bg = []
        for k, image_intensity in enumerate(self.images_intensities):
            width = len(image_intensity[0])
            height = len(image_intensity)
            cur_bg = [[0 for j in range(width)] for i in range(height)]
            for i in range(height):
                for j in range(width):
                    intensity_sum = 0
                    elements_count = 0
                    for t in range(k - self.n, k + self.n + 1):
                        if 0 <= t < len(self.images_intensities):
                            intensity_sum += self.images_intensities[t][i][j]
                            elements_count += 1
                    cur_bg[i][j] = intensity_sum / elements_count
            bg.append(cur_bg)
        return bg

    def get_seg1(self):
        seg1 = []
        bg = self.get_bg()
        for k, image in enumerate(self.images_intensities):
            width = len(image[0])
            height = len(image)
            cur_seg1 = [[0 for j in range(width)] for i in range(height)]
            for i in range(height):
                for j in range(width):
                    cur_seg1[i][j] = 1 if abs(image[i][j] - bg[k][i][j]) > self.t1 else 0
            seg1.append(cur_seg1)
        return seg1

    def get_seg2(self):
        seg1 = self.get_seg1()
        bg = self.get_bg()
        cur_sbg = bg[0]
        seg2 = []
        for k, image_intensity in enumerate(self.images_intensities):
            width = len(image_intensity[0])
            height = len(image_intensity)
            cur_seg2 = [[0 for j in range(width)] for i in range(height)]
            new_sbg = [[0 for j in range(width)] for i in range(height)]
            for i in range(height):
                for j in range(width):
                    if abs(cur_sbg[i][j] - self.images_intensities[k][i][j]) > self.t2 and seg1[k][i][j] == 1:
                        cur_seg2[i][j] = 1
                    new_sbg[i][j] = self.images_intensities[k][i][j] if cur_seg2[i][j] == 0 else cur_sbg[i][j]
            cur_sbg = new_sbg
            cur_seg2 = matrix_utils.do_majority_fix_on_01_matrix(cur_seg2)
            seg2.append(cur_seg2)
        return seg2

    def get_blobs(self):
        blobs = []
        seg2 = self.get_seg2()
        for k in range(self.size):
            cur_blobs = []
            components = find_connected_components(seg2[k], 1)
            for component in components:
                size = len(component)
                center = Point2D()
                mean_color = Point3D()
                for point in component:
                    center += point
                    rgb = self.images_rgb_matrices[k][point.x][point.y]
                    mean_color += Point3D(rgb[0], rgb[1], rgb[2])
                blob = Blob(-1, mean_color / size, center / size, component)
                if size < self.min_blob_size:
                    continue
                merged = False
                for i, to_blob in enumerate(cur_blobs):
                    if blob.can_merge_with(to_blob):
                        to_blob.merge_with(blob)
                        merged = True
                        break
                if not merged:
                    cur_blobs.append(blob)
            blobs.append(cur_blobs)
        return blobs

    def get_image_dimension(self):
        w, h = matrix_utils.get_matrix_wh(self.images_intensities[0])
        return Point2D(h, w)