__author__ = 'ilyakuchumov'


class Point2D:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def __add__(self, other):
        return Point2D(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return Point2D(self.x - other.x, self.y - other.y)

    def __truediv__(self, other):
        return Point2D(self.x / other, self.y / other)

    def __mul__(self, other):
        return Point2D(self.x * other, self.y * other)

    def __str__(self):
        return '[x = {}, y = {}]'.format(self.x, self.y)

    def __repr__(self):
        return self.__str__()


class Point3D:
    def __init__(self, x=0, y=0, z=0):
        self.x = x
        self.y = y
        self.z = z

    def __add__(self, other):
        return Point3D(self.x + other.x, self.y + other.y, self.z + other.z)

    def __sub__(self, other):
        return Point3D(self.x - other.x, self.y - other.y, self.z - other.z)

    def __truediv__(self, other):
        return Point3D(self.x / other, self.y / other, self.z / other)

    def __mul__(self, other):
        return Point3D(self.x * other, self.y * other, self.z * other)

    def __str__(self):
        return '[x = {}, y = {}, z = {}]'.format(self.x, self.y, self.z)

    def __repr__(self):
        return self.__str__()