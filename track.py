__author__ = 'ilyakuchumov'

from copy import deepcopy
import matrix_utils
from math import sqrt


class Track:
    def __init__(self, track_id, starting_frame, lost_count, center, mean_color):
        self.track_id = track_id
        self.starting_frame = starting_frame
        self.lost_count = lost_count
        self.center = center
        self.mean_color = mean_color

    def get_diff(self, blob, image_dimension):
        center_delta = blob.center - self.center
        color_delta = blob.mean_color - self.mean_color
        dist_diff = sqrt((center_delta.x / image_dimension.x)**2 + (center_delta.y / image_dimension.y)**2)
        color_diff = sqrt((color_delta.x / 255)**2 + (color_delta.y / 255)**2 + (color_delta.z / 255)**2)
        return dist_diff + color_diff


class TrackObserver:
    def __init__(self, image_dimension, max_diff=0.1, keep_alive=5):
        self.max_diff = max_diff
        self.image_dimension = image_dimension
        self.keep_alive = keep_alive

        self.track_id_ptr = 0
        self.tracks = []
        self.current_frame = -1
        self.already_matched = []

    def new_frame(self):
        self.current_frame += 1
        self.already_matched = matrix_utils.create_array(len(self.tracks), False)

    def match_blob(self, blob):
        blob = deepcopy(blob)

        track_pos = -1
        for pos, track in enumerate(self.tracks):
            if self.already_matched[pos]:
                continue
            diff = track.get_diff(blob, self.image_dimension)
            if diff > self.max_diff:
                continue
            if track_pos == -1 or (track_pos != -1 and self.tracks[track_pos].starting_frame > track.starting_frame):
                track_pos = pos

        if track_pos == -1:
            track_pos = len(self.tracks)
            new_track_id = self.track_id_ptr
            self.track_id_ptr += 1
            self.tracks.append(Track(new_track_id, self.current_frame, 0, blob.center, blob.mean_color))
            self.already_matched.append(False)

        self.already_matched[track_pos] = True
        self.tracks[track_pos].lost_count = 0
        self.tracks[track_pos].center = blob.center
        self.tracks[track_pos].mean_color = blob.mean_color

        blob.track_id = self.tracks[track_pos].track_id
        return blob

    def remove_lost_tracks(self):
        self.tracks = [track for track in self.tracks if track.lost_count < self.keep_alive]

    def match_blobs(self, blobs):
        self.new_frame()
        new_blobs = []
        for blob in blobs:
            new_blobs.append(self.match_blob(blob))
        self.remove_lost_tracks()
        return new_blobs