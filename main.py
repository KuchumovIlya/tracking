__author__ = 'ilyakuchumov'

from image_sequence import ImageSequence
from track import TrackObserver


def main():
    folder = 'single'
    image_sequence = ImageSequence('./{}'.format(folder))
    blobs_for_each_frame = image_sequence.get_blobs()
    image_dimension = image_sequence.get_image_dimension()
    track_observer = TrackObserver(image_dimension)

    for k, blobs in enumerate(blobs_for_each_frame):
        image = image_sequence.images[k]
        matched_blobs = track_observer.match_blobs(blobs)
        for blob in matched_blobs:
            image = blob.put_on_image(image)
        image.save('./marked_{}/im{}.png'.format(folder, k))


if __name__ == '__main__':
    main()