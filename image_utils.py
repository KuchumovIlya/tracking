__author__ = 'ilyakuchumov'

from PIL import Image
from copy import deepcopy
import random


def open_image(path):
    image = Image.open(path)
    pixels = list(image.getdata())
    width, height = image.size
    pixels = [pixels[i * width:(i + 1) * width] for i in range(height)]
    return image, pixels


def _get_pixel_intensity(pixel):
    r = pixel[0]
    g = pixel[1]
    b = pixel[2]
    #constants from pil documentation
    return r * 299/1000 + g * 587/1000 + b * 114/1000


def get_pixels_intensity(rgb_matrix):
    width = len(rgb_matrix[0])
    height = len(rgb_matrix)
    return [[_get_pixel_intensity(rgb_matrix[i][j]) for j in range(width)] for i in range(height)]


def create_image_from_bw_matrix(bw_matrix):
    w = len(bw_matrix[0])
    h = len(bw_matrix)
    im = Image.new('1', (w, h), 1)
    for i in range(h):
        for j in range(w):
            if bw_matrix[i][j] == 1:
                im.putpixel((j, i), (0))
    return im


def put_bw_matrix(im, bw_matrix):
    im = deepcopy(im)
    w = len(bw_matrix[0])
    h = len(bw_matrix)
    for i in range(h):
        for j in range(w):
            if bw_matrix[i][j] == 1:
                im.putpixel((j, i), (255, 255, 255))
    return im


def put_rand_points(im, points):
    im = deepcopy(im)
    color = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
    for point in points:
        im.putpixel((point.y, point.x), color)
    return im